RPN Calculator
======
**RPN Calculator** evaluates expression in RPN(reverse polish notation).

## Minimum Requirements
Java 1.8 or above is required

## Build
[Maven](https://maven.apache.org/)

## Test
mvn test

## Install
mvn install

## Run
java -jar .\target\RPNCalculator-1.0-SNAPSHOT-jar-with-dependencies.jar

## Example
![screenshot](./example.PNG)

## Author
Rongting Chen (rongting_chen@163.com) 