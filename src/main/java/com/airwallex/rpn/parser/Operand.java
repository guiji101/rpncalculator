package com.airwallex.rpn.parser;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Operand token
 */
public class Operand implements Token {

    private BigDecimal number;

    private Operand(BigDecimal number) {
        this.number = number;
    }

    public BigDecimal value() {
        return number;
    }

    static Operand of(BigDecimal number) {
        return new Operand(number);
    }

    @Override
    public boolean isOperator() {
        return false;
    }

    @Override
    public String toString() {
        return number.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Operand other = (Operand) obj;
        return Objects.equals(this.number, other.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.number);
    }

}
