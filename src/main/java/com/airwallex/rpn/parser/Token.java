package com.airwallex.rpn.parser;

/**
 * Token which is the result of parsing user input
 */
public interface Token {
    /**
     *
     * @return whether the token is operator or not
     */
    boolean isOperator();
}
