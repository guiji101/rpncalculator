package com.airwallex.rpn.parser;

public class UnrecognizedTokenException extends IllegalArgumentException {

    public UnrecognizedTokenException(String token) {
        super(String.format("Unrecognized token: %s. It is neither a number nor a valid operator", token));
    }

}
