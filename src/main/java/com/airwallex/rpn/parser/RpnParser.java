package com.airwallex.rpn.parser;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * Parser for RPN expression input
 */
public interface RpnParser {
    List<Pair<Token, Integer>> parse(String line);
}
