package com.airwallex.rpn.parser;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Parser implementation for RPN expression input
 */
public class RpnParserImpl implements RpnParser {

    private static final String SEPARATOR = "\\s";
    private static final String SPLIT_SEPARATOR = String.format("((?<=\\s)|(?=\\s))");

    public List<Pair<Token, Integer>> parse(String line){
        List<Pair<Token, Integer>> result = new LinkedList<>();
        int pos = 1;
        for (String t: line.split(SPLIT_SEPARATOR)) {
            if (!t.isEmpty() && !t.matches(SEPARATOR)) {
                result.add(ImmutablePair.of(parseToken(t), pos));
            }
            pos += t.length();
        }
        return result;
    }

    Token parseToken(String t) {
        if (Operator.operators().containsKey(t)) {
            return Operator.operators().get(t);
        } else {
            try {
                return Operand.of(new BigDecimal(t));
            } catch (NumberFormatException e) {
                throw new UnrecognizedTokenException(t);
            }
        }
    }

}
