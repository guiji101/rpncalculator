package com.airwallex.rpn.parser;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum Operator implements Token {
    Add("+", 2, true),
    Sub("-", 2, true),
    Mul("*", 2, true),
    Div("/", 2, true),
    Sqrt("sqrt", 1, true),
    Undo("undo", 0, false),
    Clear("clear", 0, false);

    /**
     * String representation of the operator
     */
    private String repr;
    /**
     * Number of arguments
     */
    private int arity;
    /**
     * whether the operator has return results
     */
    private boolean hasReturn;

    Operator(String repr, int arity, boolean hasReturn) {
        this.repr = repr;
        this.arity = arity;
        this.hasReturn = hasReturn;
    }

    /**
     * @return arity of the operator, e.g. 2(binary), 1(unary) or 0(take no parameter)
     */
    public int getArity() {
        return arity;
    }

    /**
     * @return whether the operator has return results
     */
    public boolean hasReturn() {
        return hasReturn;
    }

    @Override
    public String toString() {
        return repr;
    }

    private static final Map<String, Operator> operatorByRepr;
    static {
        operatorByRepr = Collections.unmodifiableMap(Arrays.stream(Operator.class.getEnumConstants())
                .collect(Collectors.toMap(Operator::toString, Function.identity())));
    }

    public static Map<String, Operator> operators() {
        return operatorByRepr;
    }

    @Override
    public boolean isOperator() {
        return true;
    }

}
