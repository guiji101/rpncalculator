package com.airwallex.rpn.app;

import com.airwallex.rpn.calculator.InSufficientParameterException;
import com.airwallex.rpn.calculator.RpnCalculator;
import com.airwallex.rpn.calculator.RpnCalculatorImpl;
import com.airwallex.rpn.parser.Operator;
import com.google.common.base.Joiner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.stream.Collectors;

public class RpnCalculatorApp {

    private static final String QUIT_COMMAND = "q";
    private static final String PROMPT = ">>>";

    void showUsage() {
        System.out.println("RPN Calculator");
        System.out.println("Supported operators: " + Joiner.on(",").join(
                Operator.operators().keySet().stream().sorted().collect(Collectors.toList())));
        System.out.println(String.format("Type %s to quit", QUIT_COMMAND));
    }

    public void run() throws IOException {
        showUsage();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        RpnCalculator rpnCalculator = new RpnCalculatorImpl();

        while (true) {
            System.out.print(PROMPT);
            String line = br.readLine();
            if (line.trim().equals(QUIT_COMMAND)) {
                break;
            }
            try {
                rpnCalculator.process(line);
                rpnCalculator.displayContents(System.out);
            } catch (InSufficientParameterException e) {
                System.out.println(String.format("operator %s (position: %d): insufficient parameters",
                        e.getOperator().toString(), e.getPosition()));
                rpnCalculator.displayContents(System.out);
                System.out.println("The following numbers were not pushed on to the stack due to the previous error");
                System.out.println(Joiner.on(" ").join(e.getRemainingParams()));
                break;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new RpnCalculatorApp().run();
    }

}
