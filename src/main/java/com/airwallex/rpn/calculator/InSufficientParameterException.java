package com.airwallex.rpn.calculator;

import com.airwallex.rpn.parser.Operator;

import java.math.BigDecimal;
import java.util.List;

/**
 * Indicate the error that there is not enough number in stack for an operation
 */
public class InSufficientParameterException extends IllegalStateException {

    private final Operator operator;
    private final int position;
    private final List<BigDecimal> remainingParams;

    /**
     *
     * @param operator Operator which causes this exception
     * @param position The position for operator in input line
     * @param remainingParams Remaining params not push to stack
     */
    public InSufficientParameterException(Operator operator, int position, List<BigDecimal> remainingParams) {
        super("Not enough parameter on stack.");
        this.operator = operator;
        this.position = position;
        this.remainingParams = remainingParams;
    }

    /**
     *
     * @return Operator which causes this exception
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     *
     * @return The position for operator in input line
     */
    public int getPosition() {
        return position;
    }

    /**
     *
     * @return Remaining params not push to stack
     */
    public List<BigDecimal> getRemainingParams() {
        return remainingParams;
    }
}
