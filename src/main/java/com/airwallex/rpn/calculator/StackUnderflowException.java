package com.airwallex.rpn.calculator;

class StackUnderflowException extends IllegalStateException{
    StackUnderflowException() {
        super("Trying to pop from an empty stack");
    }
}
