package com.airwallex.rpn.calculator;

import com.airwallex.rpn.parser.*;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * An implementation of RPN Calculator
 * which supports +,-,*,/,sqrt,clear and undo
 * This class is not thread safe, and is not supposed to be used in multiple threads concurrently
 */
public class RpnCalculatorImpl implements RpnCalculator {

    /**
     * used for display stack elements
     */
    private static final String SEPARATOR = " ";
    /**
     * max decimals to show when display stack elements
     */
    private static final int MAX_DECIMALS_TO_SHOW = 10;

    /**
     * precision used for calculation
     */
    private static final int PRECISION = 15;

    /**
     * parse used to parse input
     */
    private final RpnParser rpnParser;

    /**
     * parameter stack
     */
    private LinkedList<BigDecimal> stack;

    /**
     * command history, which is used for undo operation
     */
    private LinkedList<Command> commandHistory;

    /**
     * formatter used to format decimals
     */
    private DecimalFormat decimalFormat = new DecimalFormat();

    protected RpnCalculatorImpl(
            RpnParser rpnParser,
            LinkedList<BigDecimal> stack) {
        this.rpnParser = rpnParser;
        this.stack = stack;
        this.commandHistory = new LinkedList<>();
        decimalFormat.setMaximumFractionDigits(MAX_DECIMALS_TO_SHOW);
    }

    public RpnCalculatorImpl() {
        this(new RpnParserImpl(), new LinkedList<>());
    }

    /**
     * process the input expression
     * @param line input line in RPN format
     */
    @Override
    public void process(String line) {
        List<Pair<Token, Integer>> inputs = rpnParser.parse(line);
        Iterator<Pair<Token, Integer>> iter = inputs.iterator();
        Pair<Token, Integer> p;
        Token t;
        while(iter.hasNext()) {
            p = iter.next();
            t = p.getLeft();
            if (t.isOperator()) {
                try {
                    processOperator((Operator)t);
                } catch (StackUnderflowException e) {
                    List<BigDecimal> remainingParams = new LinkedList<>();
                    Operator operator = (Operator)t;
                    int position = p.getRight();
                    while(iter.hasNext()) {
                        p = iter.next();
                        t = p.getLeft();
                        if (!t.isOperator()) {
                            remainingParams.add(((Operand)t).value());
                        }
                    }
                    throw new InSufficientParameterException(operator, position, remainingParams);
                }
            }
            else
                processOperand((Operand)t);
        }
    }

    /**
     * @param operand the current operand to process
     */
    void processOperand(Operand operand) {
        stack.push(operand.value());
        commandHistory.push(new Command(Optional.empty(), ImmutableList.of()));
    }

    /**
     * @param operator the current operator to process
     */
    void processOperator(Operator operator) {
        switch (operator.getArity()) {
            case 2: processBinaryOperator(operator); break;
            case 1: processUnaryOperator(operator); break;
            case 0: processAction(operator); break;
            default: throw new IllegalStateException("Unsupported Operator Arity: " + operator.getArity());
        }
    }


    /**
     * @param operator binary operator to process
     */
    void processBinaryOperator(Operator operator) {
        if (stack.size() < 2)
            throw new StackUnderflowException();
        BigDecimal param1 = stack.pop();
        BigDecimal param2 = stack.pop();
        switch (operator) {
            case Add: stack.push(param2.add(param1)); break;
            case Sub: stack.push(param2.subtract(param1)); break;
            case Mul: stack.push(param2.multiply(param1)); break;
            case Div: stack.push(param2.divide(param1, PRECISION, BigDecimal.ROUND_HALF_EVEN)); break;
            default: throw new IllegalStateException("Unsupported Binary Operator: " + operator.getArity());
        }
        commandHistory.push(new Command(Optional.of(operator), ImmutableList.of(param1, param2)));
    }

    /**
     * @param operator unary operator to process
     */
    void processUnaryOperator(Operator operator) {
        if (stack.isEmpty())
            throw new StackUnderflowException();
        BigDecimal param = stack.pop();
        switch (operator) {
            // Once we upgrade to Java9 or later version, we can use BigDecimal#sqrt to implement this
            case Sqrt: stack.push(BigDecimal.valueOf(StrictMath.sqrt(param.doubleValue()))); break;
            default: throw new IllegalStateException("Unsupported Unary Operator: " + operator.getArity());
        }
        commandHistory.push(new Command(Optional.of(operator), ImmutableList.of(param)));
    }

    /**
     *
     * @param operator operator that takes no param
     */
    void processAction(Operator operator) {
        switch (operator) {
            case Clear: clear(); break;
            case Undo: undo(); break;
            default: throw new IllegalStateException("Unsupported Action: " + operator.getArity());
        }
    }

    /**
     * clear stack content
     */
    void clear() {
        commandHistory.push(new Command(Optional.of(Operator.Clear), ImmutableList.copyOf(stack)));
        stack.clear();
    }

    /**
     * undo the previous operation
     */
    void undo() {
        if (commandHistory.isEmpty())
            throw new NoCommandToUndoException();
        Command c = commandHistory.pop();
        c.undo();
    }

    /**
     *
     * @param out PrintStream to output calculator state to
     */
    @Override
    public void displayContents(PrintStream out) {
        StringJoiner content = new StringJoiner(SEPARATOR);
        Lists.reverse(stack).stream().map(decimalFormat::format).forEach(content::add);
        out.println("stack: " + content.toString());
    }

    /**
     * get current statck
     * @return current stack
     */
    List<BigDecimal> getStack() {
        return Collections.unmodifiableList(stack);
    }

    /**
     * command that has been processed
     */
    class Command {

        private final Optional<Operator> operator;
        private final List<BigDecimal> parameters;

        Command(Optional<Operator> operator, List<BigDecimal> parameters) {
            this.operator = operator;
            this.parameters = parameters;
        }

        void undo() {
            if (!operator.isPresent() // an operand
                    || operator.get().hasReturn())
                stack.pop();
            if (operator.isPresent()) {
                for (BigDecimal param: Lists.reverse(parameters))
                    stack.push(param);
            }
        }
    }

}
