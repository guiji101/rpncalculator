package com.airwallex.rpn.calculator;

import java.io.PrintStream;

/**
 * A calculator which processes expression in RPN (reverse polish notation) format.
 */
public interface RpnCalculator {
    /**
     * process the input expression
     * @param line input line in RPN format
     */
    void process(String line);

    /**
     * display the current stack of the calculator
     * @param out PrintStream to output calculator state to
     */
    void displayContents(PrintStream out);
}
