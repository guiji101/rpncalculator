package com.airwallex.rpn.calculator;

class NoCommandToUndoException extends IllegalStateException{
    NoCommandToUndoException() {
        super("There is not command to undo");
    }
}
