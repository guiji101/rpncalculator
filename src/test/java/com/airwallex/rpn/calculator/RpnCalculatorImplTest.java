package com.airwallex.rpn.calculator;

import com.airwallex.rpn.parser.Operator;
import com.airwallex.rpn.parser.RpnParserImpl;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RpnCalculatorImplTest {
    private RpnCalculatorImpl rpnCalculator;

    @Before
    public void setup() {
        LinkedList<BigDecimal> stack = new LinkedList<>();
        stack.push(new BigDecimal(1));
        stack.push(new BigDecimal(4));
        rpnCalculator = new RpnCalculatorImpl(new RpnParserImpl(), stack);
    }

    @Test
    public void testAddParam() {
        rpnCalculator.process("5");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(3, stack.size());
        assertEquals(new BigDecimal(5), stack.get(0));
        assertEquals(new BigDecimal(4), stack.get(1));
        assertEquals(new BigDecimal(1), stack.get(2));
    }

    @Test
    public void testRunBinaryOp() {
        rpnCalculator.process("+");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(1, stack.size());
        assertEquals(new BigDecimal(5), stack.get(0));
    }

    @Test
    public void testBinaryOpParamOrder(){
        rpnCalculator.process("-");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(1, stack.size());
        assertEquals(new BigDecimal(-3), stack.get(0));
    }

    @Test
    public void testDiv() {
        rpnCalculator.process("1 3 /");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(3, stack.size());
        assertEquals(1.0 / 3, stack.get(0).doubleValue(), 1.0e-15);
    }

    @Test
    public void testRunUnaryOp() {
        rpnCalculator.process("sqrt");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(2, stack.size());
        assertEquals(2.0, stack.get(0).doubleValue(), 1.0e-3);
        assertEquals(new BigDecimal(1), stack.get(1));
    }

    @Test
    public void testClear() {
        rpnCalculator.process("clear");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(0, stack.size());
    }

    @Test
    public void testUndo() {
        rpnCalculator.process("+ undo");
        List<BigDecimal> stack = rpnCalculator.getStack();
        assertEquals(2, stack.size());
        assertEquals(new BigDecimal(4), stack.get(0));
        assertEquals(new BigDecimal(1), stack.get(1));
    }
}
