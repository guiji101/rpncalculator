package com.airwallex.rpn.parser;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class RpnParserImplTest {

    private final RpnParserImpl rpnParser = new RpnParserImpl();

    @Test
    public void testParseOprand() {
        Token t = rpnParser.parseToken("1");
        assertEquals(Operand.class, t.getClass());
        assertEquals(new BigDecimal(1), ((Operand)t).value());
    }

    @Test
    public void testParseOperator() {
        Map<String, Operator> operators = Operator.operators();
        for (Map.Entry<String, Operator> entry: operators.entrySet()) {
            Token t = rpnParser.parseToken(entry.getKey());
            assertEquals(Operator.class, t.getClass());
            assertEquals(entry.getValue(), t);
        }
    }

    @Test
    public void testParseEmpty() {
        List<Pair<Token, Integer>> result = rpnParser.parse("");
        assertEquals(result.size(), 0);
    }

    @Test
    public void testParseLine() {
        List<Pair<Token, Integer>> result = rpnParser.parse("1 2  +");
        assertEquals(3, result.size());
        assertEquals(new BigDecimal(1), ((Operand)result.get(0).getLeft()).value());
        assertEquals(new BigDecimal(2), ((Operand)result.get(1).getLeft()).value());
        assertEquals("+", ((Operator)result.get(2).getLeft()).toString());

        assertEquals(Integer.valueOf(1), result.get(0).getRight());
        assertEquals(Integer.valueOf(3), result.get(1).getRight());
        assertEquals(Integer.valueOf(6), result.get(2).getRight());
    }
}
